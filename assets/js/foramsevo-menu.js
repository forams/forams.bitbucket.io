var paramNumber = 7;
var mutationParamNumber = 6;
var prefix = 'param';

var isWebGLOn = false;
var debugMode = false;
var generation = 1;
var debugLegendDiv;
var debugConsole;
var lastSimParams = [0.4, 1.1, 71, 30, 5, 0.5, 10];
var showDebugLegend = false;
var noSelectedShellsMessage = "<p>Select at least one shell and then press button";
var isFirefox = /Firefox/i.test(navigator.userAgent);
var numberOfShells = 1;
var selectAllFlag = true;

function startNew(type, parent1, parent2, parameters) {
	if(type != 'debug') debugMode = false;
	debugLegendDiv = document.getElementById('debug_legend');
	consoleDiv = document.getElementById('console_container');
	debugConsole = document.getElementById('console');
	if (debugMode) {
		document.getElementById('calc').style.display = 'none';
		document.getElementById('debug_button').style.visibility = 'visible';
		debugLegendDiv.style.display = "block";
		consoleDiv.style.display = "block";
		document.getElementById('generation').style.display = 'none';
		document.getElementById('cp_button').style.display = 'none';
		document.getElementById('select_all_button').style.display = 'none';
	}else{
		document.getElementById('calc').style.display = 'block';
		document.getElementById('debug_button').style.visibility = 'hidden';
		debugLegendDiv.style.display = "none";
		consoleDiv.style.display = "none";
		document.getElementById('generation').style.display = 'inline';
		document.getElementById('cp_button').style.display = 'inline';
		updateSelectButton();
		document.getElementById('select_all_button').style.display = 'inline';
	}	
	if(isWebGLOn==false) {
		removeHeader();
		init();
		animateScene();
		isWebGLOn = true;
	}
	clearScene();
	isInspecting = false;
	infoText.innerHTML = "";
	selectedShells = [];
	if(type != 'debug') backupForams = [];
	var fl=0;
	var options = new Array();

	for(i = 1; i<=paramNumber; i++) {	
		x = document.getElementById(prefix+i.toString()).value;
		if(isNaN(x)) {
			fl=1;
		}
		if(fl==1) break;
		options.push( parseFloat(x));
	}
	var menuOptions = options.slice();
	options[2]=options[2]*(Math.PI/180);
	options[3]=options[3]*(Math.PI/180);
	options[4]=options[4]/radiusScale;
	// combobox value
	var combobox = document.getElementById("num_of_shells");
	var selectedItem = combobox.options[combobox.selectedIndex].text;

	if(selectedItem=="1x1") numberOfShells = 1;
	if(selectedItem=="2x2") numberOfShells = 2;
	if(selectedItem=="3x3") numberOfShells = 3;
	if(selectedItem=="4x4") numberOfShells = 4;
	
	if(fl==1) alert("Invalid options.");
	else{
		var children = [];
		if(type=="mutation") {
			children = mutateForam(parent1, numberOfShells*numberOfShells);
		} else if (type=="crossbreed") {
			children = crossbreedForam([parent1, parent2], numberOfShells*numberOfShells);
		} else if (type=="debug") {
			children = parameters;
			if(!debugMode) numberOfShells = Math.sqrt(children.length);
		} else {
			lastSimParams = menuOptions;
			for(var i = 0; i < numberOfShells*numberOfShells; i++){
				children[i] = options;
			}
		}
		
		if(!debugMode){
			$("#param7").attr("disabled", false);
			if(numberOfShells == 1){
				$("#debug_mode").removeAttr("disabled");
				$("#debug_mode").attr("checked", false);
			} else {
				$("#debug_mode").attr("disabled", true);
				$("#debug_mode").attr("checked", false);
			}
		}
		
		var transpValue = document.getElementById('transp_slider').value;
		var drawPath = $('#min_path').is(":checked");
		var schemaOn = $('#color_schema').is(":checked");
		if(debugMode) {
			$("#param7").attr("disabled", true);
			enterDebugMode(children[0], drawPath, transpValue);
		}

		else addForams(children, numberOfShells, drawPath, transpValue, schemaOn);
	}
	document.getElementById("webgl_frame").focus();
}

function onClickReset(){
	var newParams = lastSimParams.slice();
	var chamberCount = newParams.pop();
	setMenuParams(newParams);
	document.getElementById("param7").value = chamberCount;	//ilosc komor
	document.getElementById("webgl_frame").focus();
}

function onClickResetRotation(){
	for(i=0; i<forams.length; i++) {
		forams[i].rotation.x = 3.14;
		forams[i].rotation.y = 0;
		forams[i].rotation.z = 0;
	}
	document.getElementById("webgl_frame").focus();
}

function toggleDebugLegend(){
	var table = document.getElementById('debug_legend_table');
	var button = document.getElementById('show_legend_button');
	if(showDebugLegend) {
		table.style.display = 'block';
		button.style.display = 'none';
	} else {
		table.style.display = 'none';
		button.style.display = 'block';
	}
	showDebugLegend = !showDebugLegend;
}

function onClickResetDebug(){
	stepCounter = 0;
	var params = debuggedForam.options.slice();
	clearScene();
	var transpValue = document.getElementById('transp_slider').value;
	var drawPath = $('#min_path').is(":checked");
	enterDebugMode(params, drawPath, transpValue);
	document.getElementById("webgl_frame").focus();
}

function setMenuParams(parameters){
	for(var i = 1; i < paramNumber; i++){
		document.getElementById("param"+i).value=parameters[i-1];
	}
}

function getMutationRates(){
	var checked;
	var mutationRates = [];
	for(var i = 1; i<=mutationParamNumber; i++) {
		checked = document.getElementById("mut"+i).checked;
		if(checked){
			var rate = document.getElementById("mutRange"+i).value;
			mutationRates.push(rate);
		} else {
			mutationRates.push(0);
		}
	}
	return mutationRates;
}
function resetGeneration(){
	generation = 1;
	document.getElementById("generation").innerHTML="Generation: 1"; 
}
function getParameters() {
	//TODO

	document.getElementById("webgl_frame").focus();
}

function removeHeader() {
	document.getElementById('header').style.display = 'none';
	document.getElementById('footer').style.display = 'none';
	document.getElementById('logo_mini').style.display = 'inline';
	 //document.getElementById('WebGLCanvas').style.background = '#141F29';
	document.getElementById('WebGLCanvas').className = document.getElementById('WebGLCanvas').className + "gradient";
	document.body.className = "gradient";
}

$("document").ready(function() {
	$("#transp_slider").on("change", function(){
		var sliderValue = document.getElementById('transp_slider').value;
		if(debugMode){
			for(var i = 0; i < debugForamChambers.length; i++){
				debugForamChambers[i].material.opacity = sliderValue;
			}
		} else {
			for(var i = 0; i < forams.length; i++){
				for(var j = 0; j < forams[i].children.length - 1; j++){
					forams[i].children[j].material.opacity = sliderValue;
				}
			}
		}
		document.getElementById("webgl_frame").focus();
	});
	$("#debug_mode").click(function() {
        if ($(this).is(':checked')) {
			debugMode = true;
			document.querySelector('#transp_slider').value = 0.2;
			var params = forams[0].userData.foram.options;
			setMenuParams([round(params[0], 4),round(params[1], 4),round(params[2] * 180.0 / Math.PI, 4),round(params[3] * 180.0 / Math.PI, 4), round(params[4] * radiusScale, 4), round(params[5], 4)]);
			if(isInspecting){
				startNew('debug', null, null, [forams[0].userData.foram.options]);
			} else {
				backupForams.push(forams[0]);
				startNew('debug', null, null, [backupForams[0].userData.foram.options]);
			}
        } else {
			debugMode = false;
			document.querySelector('#transp_slider').value = 1;
			var parameters = [];
			for(var i = 0; i < backupForams.length; i++){
				parameters.push(backupForams[i].userData.foram.options);
			}
			backupForams = [];
			startNew('debug', null, null, parameters);
		}
		document.getElementById("webgl_frame").focus();
    });
	$("#min_path").click(function() {
        if ($(this).is(':checked')) {
			if(debugMode){
				addDebugMinPath();
			} else {
				addMinPaths();
			}
        } else {
			if(debugMode){
				debuggedGroup.remove(debugMinPath);
				debugMinPath = null;
			} else {
				removeMinPaths();
			}
		}
		document.getElementById("webgl_frame").focus();
    });
	$("#color_schema").click(function() {
        if ($(this).is(':checked')) {
			if(!debugMode){
				var foramList = forams.concat(backupForams) 
				applySchemaColors(foramList);
			}
        } else {
			if(!debugMode){
				var foramList = forams.concat(backupForams) 
				setDefaultColorSchema(foramList);
			}
		}
		document.getElementById("webgl_frame").focus();
    });
	$(".validated_input").change(function() {
		var value = parseFloat($(this).val());
		var max = parseFloat($(this).attr('max'));
		var min = parseFloat($(this).attr('min'));
		if(!value) {
			if(this.id == 'param5') {
				var thickness = parseFloat($("#param6").val());
				value = thickness + 0.001;
				$(this).val(value);
			}
			else $(this).val(min);
		} else {
			if(this.id == 'param6'){
				var radius = parseFloat($("#param5").val());
				if(value >= radius) {
					value = radius - 0.001;
					$(this).val(round(value,4));
				}
			}
			else if(this.id == 'param5'){
				var thickness = parseFloat($("#param6").val());
				if(value <= thickness) {
					value = thickness + 0.001;
					$(this).val(round(value, 4));
				}
			}
			if(value > max) $(this).val(max);
			else if(value < min) $(this).val(min);
			else if(this.id == 'param7') $(this).val(Math.floor(value));
		}
	});
	$("input[type='number']").on('mousewheel DOMMouseScroll', function(event){
		event.preventDefault();
		var value = parseFloat($(this).val());
		var step = parseFloat($(this).attr('step'))
		if(isFirefox){
			if(event.originalEvent.detail < 0){
				var max = parseFloat($(this).attr('max'));
				var newValue = round(value + step,4);
				if(newValue <= max) $(this).val(newValue).change();
			} else {
				var min = parseFloat($(this).attr('min'));
				var newValue = round(value - step,4);
				if(newValue >= min) $(this).val(newValue).change();
			}
		} else {
			if(event.originalEvent.wheelDelta > 0){
				var max = parseFloat($(this).attr('max'));
				var newValue = round(value + step,4);
				if(newValue <= max) $(this).val(newValue).change();
			} else {
				var min = parseFloat($(this).attr('min'));
				var newValue = round(value - step,4);
				if(newValue >= min) $(this).val(newValue).change();
			}
		}
	})
});

function updateLightIntensivity(value) {
	if(spotLight) spotLight.intensity = parseFloat(value);
	document.getElementById("webgl_frame").focus();
}

function updateLightColor(value) {
	if(spotLight) spotLight.color.setHex(parseInt("0x"+value));
	document.getElementById("webgl_frame").focus();
}

function selectAll() {
	if(selectAllFlag) {
		for(var i=0; i<forams.length; i++) {
			selectedShells.push(forams[i]);
			changeShellColor(forams[i], "green");

		}
	} else {
		var selectedList = selectedShells.slice();
		selectedShells = [];
		for(var i=0; i<selectedList.length; i++) {
			changeShellColor(selectedList[i], "white");
		}
	}
	updateSelectButton();
	document.getElementById("webgl_frame").focus();
}

function updateSelectButton() {
	if(selectedShells.length>0) {
		document.getElementById("select_all_button").innerHTML =  "Deselect all";
		selectAllFlag = false;
	} else {
		document.getElementById("select_all_button").innerHTML =  "Select all";
		selectAllFlag = true;
	}
	updateUpperText();
}

$(function(){

	$("#panel").css("left","-220px");
	$("#more_panel").css("left","-241px");

	$(".slide_button").toggle(function(){

	$("#more_panel").animate({left: "0px"}, 500 );
	$("#panel").animate({left: "0px"}, 500 );
	$(this).addClass("zamknij"); 
	return false;
	},
function(){	
		$("#panel").animate({left: "-220px"}, 500 );
		$("#more_panel").animate({left: "-240px"}, 500 );
		$(this).removeClass("zamknij"); 
		return false;
	});

});


$(function(){
			
	$(".slide_button_more").toggle(function(){

	$("#more_panel").animate({left: "220px"}, 500 );
	$(this).addClass("zamknij_more"); 
	return false;
	},
	function(){	
		$("#more_panel").animate({left: "0px"}, 500 );
		$(this).removeClass("zamknij_more"); 
		return false;
	});

});	

$(function(){

	$("#info_panel").css("left","-220px");
			
	$(".slide_button_info").toggle(function(){

	$("#info_panel").animate({left: "0px"}, 500 );
	$(this).addClass("zamknij_info"); 
	return false;
	},
	function(){	
		$("#info_panel").animate({left: "-220px"}, 500 );
		$(this).removeClass("zamknij_info"); 
		return false;
	});

});	

$(function(){

	$("#control_panel").css("left","-220px");

	$(".slide_button_control").toggle(function(){

	$("#control_panel").animate({left: "0px"}, 500 );
	$(this).addClass("zamknij_control"); 
	return false;
	},
	function(){	
		$("#control_panel").animate({left: "-220px"}, 500 );
		$(this).removeClass("zamknij_control"); 
		return false;
	});

});	

function updateTextInput(id, val) {
	var element = document.getElementById(id);
	if(element) element.value = val; 
}

function round(n, k)
{
    var factor = Math.pow(10, k+1);
    n = Math.round(Math.round(n*factor)/10);
    return n/(factor/10);
}

function getVolumeWithMaxUnit(number){
	if(number > Math.pow(10.0, 18)) return [number/Math.pow(10.0, 18), ' m'];
	else if(number > Math.pow(10.0, 15)) return [number/Math.pow(10.0, 15), ' dm'];
	else if(number > Math.pow(10.0, 12)) return [number/Math.pow(10.0, 12), ' cm'];
	else if(number > Math.pow(10.0, 9)) return [number/Math.pow(10.0, 9), ' mm'];
	else return [number, ' &#181;m'];
}

function getAreaWithMaxUnit(number){
	if(number > Math.pow(10.0, 12)) return [number/Math.pow(10.0, 12), ' m'];
	else if(number > Math.pow(10.0, 10)) return [number/Math.pow(10.0, 10), ' dm'];
	else if(number > Math.pow(10.0, 8)) return [number/Math.pow(10.0, 8), ' cm'];
	else if(number > Math.pow(10.0, 6)) return [number/Math.pow(10.0, 6), ' mm'];
	else return [number, ' &#181;m'];
}


(function($) {
    $(function() {           
        $('#calc').bind('click', function(e) {
            e.preventDefault();
			if(isInspecting) selectedShells.push(forams[0]);
			var content = "<table class=\"dtable\"><tr class=\"dtr\"><th class=\"dth\"></th><th class=\"dth\">Material Volume</th><th class=\"dth\">Surface Area</th><th class=\"dth\">Total Volume</th><th class=\"dth\">Min. Path proportion</th></tr>";

			// copy list is required to fill the table
			var selectedShellsSorted = [];
			for(var i = 0; i<selectedShells.length; i++) {
				selectedShellsSorted.push(selectedShells[i]);
			}
			// use custom compare function to sort list
			selectedShellsSorted.sort(comparePositionOnGrid);

			for(var i = 0; i < selectedShellsSorted.length; i++){	
				var userData = selectedShellsSorted[i].userData;
				if(!userData.quantities){
					var values = userData.foram.calculateMaterialVolumeAndArea();
					var totalVolume = userData.foram.calculateTotalVolume();
					var pathProportion = userData.foram.calculatePathProportion();
					userData.quantities = [values[0], values[1], totalVolume, pathProportion];
				}
				var displayedQuantities = [getVolumeWithMaxUnit(userData.quantities[0]), getAreaWithMaxUnit(userData.quantities[1]),
									getVolumeWithMaxUnit(userData.quantities[2]), userData.quantities[3]];
				
				content = content + "<tr class=\"dtr\"><td class=\"dtd\">"+icon(numberOfShells, userData.posOnScreen[0]+1, numberOfShells-userData.posOnScreen[1])+"</td>" + 
							"<td class=\"dtd\">" + round(displayedQuantities[0][0],4) + displayedQuantities[0][1] + "3".sup() + "</td>" +
							"<td class=\"dtd\">" + round(displayedQuantities[1][0],4) + displayedQuantities[1][1] + "2".sup() + "</td>" +
							"<td class=\"dtd\">" + round(displayedQuantities[2][0],4) + displayedQuantities[2][1] + "3".sup() + "</td>" +
							"<td class=\"dtd\">" + round(displayedQuantities[3],4) + "</td></tr>";
			}
			content = content + "</table>";
			if(selectedShells.length == 0) $("#element_to_pop_up").html(noSelectedShellsMessage);
			else $("#element_to_pop_up").html(content);
			if(isInspecting) selectedShells.pop();
        $('#element_to_pop_up').bPopup(); 
    	
        });
    });
})(jQuery);

function icon(size, x, y) {
	// var table = "<table class=\"table_icon\" style=\"margin-left:-"+((size*10+3)/2)+"px; margin-top:-"+((size*10+3)/2)+"px;\">\n";
	var table = "<table class=\"table_icon\">\n";
	for(var i=0; i<size; i++) {
		table += "\t<tr class=\"tr_icon\">\n";
		for(var j=0; j<size; j++) {
			if(i==y-1 && j==x-1) {
				table += "\t\t<td class=\"td_icon_marked\">\n";
			} else {
				table += "\t\t<td class=\"td_icon\" >\n";
			}
			table += "\t\t</td>\n";
		}
		table += "\t</tr>\n";
	}
	table += "</table>";
	return table;
}

(function($) {
    $(function() {           
        $('#cp_button').bind('click', function(e) {
            e.preventDefault();		
			if(isInspecting) selectedShells.push(forams[0]);
			var content = "<table class=\"dtable\"><tr class=\"dtr\"><th class=\"dth\"></th><th class=\"dth\">TF</th><th class=\"dth\">GF</th><th class=\"dth\">&Delta;&Phi;&deg;</th><th class=\"dth\">&beta;&deg;</th><th class=\"dth\">R[&#181;m]</th><th class=\"dth\">Thick.[&#181;m]</th></tr>";
			
			// copy list is required to fill the table
			var selectedShellsSorted = [];
			for(var i = 0; i<selectedShells.length; i++) {
				selectedShellsSorted.push(selectedShells[i]);
			}
			// use custom compare function to sort list
			selectedShellsSorted.sort(comparePositionOnGrid);

			for(var i = 0; i < selectedShellsSorted.length; i++){	
				var userData = selectedShellsSorted[i].userData;	
				content = content + "<tr class=\"dtr\"><td class=\"dtd\">"+icon(numberOfShells, userData.posOnScreen[0]+1, numberOfShells-userData.posOnScreen[1])+"</td>" + 
							"<td class=\"dtd\">" + round(userData.foram.options[0], 4) + "</td>" +
							"<td class=\"dtd\">" + round(userData.foram.options[1], 4) + "</td>" +
							"<td class=\"dtd\">" + round(userData.foram.options[2]*180.0/Math.PI, 4) + "</td>" +
							"<td class=\"dtd\">" + round(userData.foram.options[3]*180.0/Math.PI, 4) + 
							"<td class=\"dtd\">" + round(userData.foram.options[4]*radiusScale, 4) + 
							"<td class=\"dtd\">" + round(userData.foram.options[5], 4) + "</td></tr>";
			}
			content = content + "</table>";
			if(selectedShells.length == 0) $("#element_to_pop_up2").html(noSelectedShellsMessage);
			else $("#element_to_pop_up2").html(content);
			if(isInspecting) selectedShells.pop();
			$('#element_to_pop_up2').bPopup(); 
		});
    });
})(jQuery);

function comparePositionOnGrid(foram1, foram2) {
	var userData1 = foram1.userData;
	var userData2 = foram2.userData;
	var pos1 = (2-userData1.posOnScreen[1])*numberOfShells+userData1.posOnScreen[0];
	var pos2 = (2-userData2.posOnScreen[1])*numberOfShells+userData2.posOnScreen[0];
	return pos1-pos2;
}