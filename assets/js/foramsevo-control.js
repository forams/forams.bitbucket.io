/****************************/
/********* CONTROLS *********/
/****************************/

var keyboard = new THREEx.KeyboardState();
var grabbedShell = [null, null, null];
var mouseDown = false;
var prevX = -1;
var prevY = -1;

var positionOfMouseDown = [null, null];
var positionOfMouseUp = [null, null];

var isInspecting = false;
var backupForams = [];
var oldShellParams = [];
var inspectMinPath = false;
var lastClickedShell = null;
var selectedShells = [];

function onMouseMove(event) {
	event.preventDefault();
	if(prevX==-1)
		prevX=event.clientX;
	if(prevY==-1)
		prevX=event.clientY;
	if(keyboard.pressed("ctrl") && mouseDown && event.button==0) {
		if(prevX<event.clientX) {
			for(i=0; i<forams.length; i++) {
				forams[i].rotation.y -= (event.clientX-prevX)/150;
			}
		}
		if(prevX>event.clientX) {
			for(i=0; i<forams.length; i++) {
				forams[i].rotation.y += (prevX-event.clientX)/150;
			}
		}
		if(prevY<event.clientY){
			for(i=0; i<forams.length; i++) {
				forams[i].rotation.x += (event.clientY-prevY)/150;
			}
		}
		if(prevY>event.clientY){
			for(i=0; i<forams.length; i++) {
				forams[i].rotation.x -= (prevY-event.clientY)/150;
			}
		}
		prevX = event.clientX;
		prevY = event.clientY;
	}else if(mouseDown && grabbedShell!=null) {
		if(prevX<event.clientX) {
			grabbedShell.rotation.y -= (event.clientX-prevX)/150;
		}
		if(prevX>event.clientX) {
			grabbedShell.rotation.y += (prevX-event.clientX)/150;    
		}
		if(prevY<event.clientY){
			grabbedShell.rotation.x += (event.clientY-prevY)/150;
		}
		if(prevY>event.clientY){
			grabbedShell.rotation.x -= (prevY-event.clientY)/150;
		}
		prevX = event.clientX;
		prevY = event.clientY;
	}
	
	lastMouseMove = date.getTime();
	mousePosition = [event.clientX, event.clientY];

}

function onMouseDown(event) {
	event.preventDefault();
	positionOfMouseDown = [event.clientX, event.clientY]
	if(event.button==0) mouseDown = true;
	prevX = event.clientX;
	prevY = event.clientY;
	grabbedShell = mouseDownOnShell(event.clientX, event.clientY)[0];
}

function onMouseUp(event) {
	event.preventDefault();
	document.getElementById('shell_color').color.hidePicker();
	document.getElementById("webgl_frame").focus();
	if(event.button==0) mouseDown = false;
	positionOfMouseUp = [event.clientX, event.clientY];
	if(!debugMode){
		if(keyboard.pressed("shift") && positionOfMouseUp[0] != null && positionOfMouseUp[0]==positionOfMouseDown[0] && positionOfMouseUp[1]==positionOfMouseDown[1] && grabbedShell!=null) {
			inspectShell(grabbedShell);
		} else if(positionOfMouseUp[0] != null && positionOfMouseUp[0]==positionOfMouseDown[0] && positionOfMouseUp[1]==positionOfMouseDown[1] && grabbedShell!=null && isInspecting==false) {
			if(grabbedShell.userData.color == "white") {
				selectedShells.push(grabbedShell);
				changeShellColor(grabbedShell, "green");
				var params = grabbedShell.userData.foram.options;
				setMenuParams([round(params[0], 4),round(params[1], 4),round(params[2] * 180.0 / Math.PI, 4),round(params[3] * 180.0 / Math.PI, 4), round(params[4] * radiusScale, 4), round(params[5], 4)]);
			} else {
				remove(selectedShells, grabbedShell);
				changeShellColor(grabbedShell, "white");
			}
			updateSelectButton();
		}
	}
}

function updateUpperText() {
	if(isInspecting==true) {
		infoText.innerHTML = "Shift + LMB to leave inspection mode";
	} else if (debugMode) {
		infoText.innerHTML = "Press 'space' for next step.";
	} else if(selectedShells.length==1) {
		infoText.innerHTML = "Press 'space' to mutate.";
	} else if (selectedShells.length==2) {
		infoText.innerHTML = "Press 'space' to crossbreed.";
	} else {
		infoText.innerHTML = "";
	}
}

function onMousewheel (event) {
	if(event.wheelDelta>0) { 
		for(i=0; i<forams.length; i++) {
			if((forams[i].scale.x*1.05<forams[i].userData.x_maxScale  &&
			    forams[i].scale.y*1.05<forams[i].userData.y_maxScale  &&
			    forams[i].scale.z*1.05<forams[i].userData.z_maxScale) ||
			    debugMode) {
				forams[i].scale.x *= 1.05;
				forams[i].scale.y *= 1.05;
				forams[i].scale.z *= 1.05;
			}
		}
	} else { 
		for(i=0; i<forams.length; i++) {
			if(forams[i].scale.x*0.95>=0.001 &&
			   forams[i].scale.y*0.95>=0.001 &&
			   forams[i].scale.y*0.95>=0.001) {
				forams[i].scale.x *= 0.95;
				forams[i].scale.y *= 0.95;
				forams[i].scale.z *= 0.95;
			}
		}
	}
}

function onMousewheelFirefox(event) {
	if(event.detail<0) { 
		for(i=0; i<forams.length; i++) {
			if((forams[i].scale.x*1.05<forams[i].userData.x_maxScale  &&
			    forams[i].scale.y*1.05<forams[i].userData.y_maxScale  &&
			    forams[i].scale.z*1.05<forams[i].userData.z_maxScale) ||
			    debugMode || isInspecting) {
				forams[i].scale.x *= 1.05;
				forams[i].scale.y *= 1.05;
				forams[i].scale.z *= 1.05;
			}
		}
	} else { 
		for(i=0; i<forams.length; i++) {
			if(forams[i].scale.x*0.95>=0.001 &&
			   forams[i].scale.y*0.95>=0.001 &&
			   forams[i].scale.y*0.95>=0.001) {
				forams[i].scale.x *= 0.95;
				forams[i].scale.y *= 0.95;
				forams[i].scale.z *= 0.95;
			}
		}
	}
}

function onKeyPressed(event) {

	if ( keyboard.pressed("a") )
		for(i=0; i<forams.length; i++)
			forams[i].rotation.y -= 0.1;

	if ( keyboard.pressed("d") )
		for(i=0; i<forams.length; i++)
			forams[i].rotation.y += 0.1;

	if ( keyboard.pressed("w") )
		for(i=0; i<forams.length; i++)
			forams[i].rotation.x -= 0.1;

	if ( keyboard.pressed("s") )
		for(i=0; i<forams.length; i++)
			forams[i].rotation.x += 0.1;

	if(debugMode){
		if(event.keyCode==32) {
			infoText.innerHTML = "";
			var drawPath = $('#min_path').is(":checked");
			var transpValue = document.getElementById('transp_slider').value;
			nextDebugStep(drawPath, transpValue);
			selectedShells = [];
		}
	}else{
		if(event.keyCode==32 && selectedShells.length==2) {
			infoText.innerHTML = "";
			startNew("crossbreed", selectedShells[0].userData.foram.options, selectedShells[1].userData.foram.options, null);
			selectedShells = [];
		}
		if(event.keyCode==32 && selectedShells.length==1) {
			infoText.innerHTML = "";
			startNew("mutation", selectedShells[0].userData.foram.options, null, null);
			selectedShells = [];
		}
	}
	lastMouseMove = date.getTime();
}

function changeShellColor(shell, color) {
	if(color=="white") {
		var schemaOn = $('#color_schema').is(":checked");
		if(schemaOn) applySchemaColors([shell]);
		else setDefaultColorSchema([shell]);
		shell.userData.color = "white";
	}
	if(color=="green") {
		for(child in shell.children) {
			if(shell.children[child].userData.type == 'chamber')
			shell.children[child].material.color.setHex( 0x00FF00 );
		}
		shell.userData.color = "green";
	}
}

function inspectShell(shell) {
	if(forams.length>1 || isInspecting)
	if(isInspecting==false) {
		$("#debug_mode").removeAttr("disabled");
		$("#debug_mode").attr("checked", false);
		document.getElementById("select_all_button").style.display = "none";
		updateSelectButton();
		inspectMinPath = $('#min_path').is(":checked");
		selectedShells = [];
		for(i=0; i<forams.length; i++) {
			changeShellColor(forams[i], "white");
			backupForams.push(forams[i]);
		}
		clearScene();
		oldShellParams = [shell, shell.position.x, shell.position.y, shell.scale.x, shell.scale.y, shell.scale.z,
						  shell.userData.x_maxScale, shell.userData.y_maxScale, shell.userData.z_maxScale,
						  shell.rotation.x, shell.rotation.y, shell.rotation.z];
		
		shell.userData.bbox = new THREE.Box3().setFromObject(shell);

		setGroupCoordinates(shell, 1, 0, 0);
		forams.push(shell);
		scene.add(shell);
		isInspecting = true;
	} else {
		$("#debug_mode").attr("disabled", true);
		$("#debug_mode").attr("checked", false);
		document.getElementById("select_all_button").style.display = "inline";
		updateSelectButton();
		var drawPath = $('#min_path').is(":checked");
		var pathAction;
		if(!inspectMinPath && drawPath) pathAction = 'add';
		else if(inspectMinPath && !drawPath) pathAction = 'remove';
		selectedShells = [];
		clearScene();
		var opacity = document.getElementById("transp_slider").value;
		while(backupForams.length!=0) {
			nextShell = backupForams.pop();
			if(nextShell==oldShellParams[0]) {
				shell.position.x = oldShellParams[1];
				shell.position.y = oldShellParams[2];
				shell.scale.x = oldShellParams[3];
				shell.scale.y = oldShellParams[4];
				shell.scale.z = oldShellParams[5];
				shell.userData.x_maxScale = oldShellParams[6];
				shell.userData.y_maxScale = oldShellParams[7];
				shell.userData.z_maxScale = oldShellParams[8];
				shell.rotation.x = oldShellParams[9];
				shell.rotation.y = oldShellParams[10];
				shell.rotation.z = oldShellParams[11];
				for(var i=0; i<shell.children.length; i++) {
					if(shell.children[i].userData.type == "chamber") shell.children[i].material.opacity = opacity;
				}
				oldShellParams = [];
			}
			for(var i=0; i<nextShell.children.length; i++) {
				if(nextShell.children[i].userData.type == "chamber") nextShell.children[i].material.opacity = opacity;
			}
			forams.push(nextShell);
			scene.add(nextShell);
		}
		if(pathAction == 'add') addMinPaths();
		else if(pathAction == 'remove') removeMinPaths();
		isInspecting = false;
	}
	updateUpperText();
}

function mouseDownOnShell(posX, posY) {
	mouse.x = ( posX / renderer.domElement.width ) * 2 - 1;
	mouse.y = - ( posY / renderer.domElement.height ) * 2 + 1;
	raycaster.setFromCamera( mouse, camera );
	var intersects = raycaster.intersectObjects(forams, true);

	// 
	// 	INTERSECTING DEBUG
	//	   (TO REMOVE) 
	// 
	// if(intersects.length) {
	// 	if(intersects[0].object.userData.name != undefined) {
	// 		console.log("grabbed: "+intersects[0].object.userData.name)
	// 	} else {
	// 		if(intersects[0].object.userData.p == undefined)
	// 			console.log("grabbed something but it doesn't have parent!");
	// 		else
	// 			console.log("grabbed something unnamed but with parent");
	// 	}
	// } else {
	// 	console.log("grabbed nothing");
	// }

	if(intersects.length) {
		return [intersects[0].object.userData.p, posX, posY];
	} else {
		return [null, null, null];
	}

}

function onWindowResize(event) {
	renderer.setSize(window.innerWidth-20, window.innerHeight-5);
	camera = new THREE.PerspectiveCamera(45, (window.innerWidth-20)/window.innerHeight, 1, 100);//to fix
	camera.position.set(0,0,10);
	camera.lookAt(scene.position);
	scene.add(camera);
	renderScene();
}

function createInfoText() {
	infoText = document.createElement('div');
	infoText.style.position = 'absolute';
	infoText.style.fontFamily = 'Times New Roman';
	infoText.style.fontSize = 40+'px';
	infoText.style.color = "gray";
	infoText.innerHTML = "";
	infoText.style.top = 10 + 'px';
	infoText.style.left = 250+'px';
	document.body.appendChild(infoText);
}