/* CONST */
var DESC_WINDOW_DELAY = 500;

/* GLOBAL VARIABLES */
var scene, camera, container, raycaster, mouse;
var forams = [];
var infoText = "";
var shellDescriptionWindow = null;
var lastMouseMove = null;
var date = null;
var spotLight;
var mousePosition = [null, null];
var schemaColors = [0xFF0000,0x00FF00,0xFFFF00,0xff69b4,0x0000ff,0xffa500];

// initialize three.js, canvas, set up scene, lighting etc.
function init() {
	// does browser support WebGL?
	if(Detector.webgl) renderer = new THREE.WebGLRenderer({antialias:true, alpha:true});
	else renderer = new THREE.CanvasRenderer();

	// init renderer
	renderer.setClearColor(0x000000, 0);
	renderer.setSize(window.innerWidth -20, window.innerHeight-5);
	renderer.shadowMapEnabled = true;
	renderer.sortObjects = false;
	
	// place renderer on html tag
	container = document.getElementById("WebGLCanvas").appendChild(renderer.domElement);

	// init scene
	scene = new THREE.Scene();

	// camera
	camera = new THREE.PerspectiveCamera(45, (window.innerWidth-20)/window.innerHeight, 1, 100); //to fix
	camera.position.set(0,0,10);
	camera.lookAt(scene.position);
	scene.add(camera);
	
	// light
	spotLight = new THREE.SpotLight(parseInt("0x"+document.getElementById('shell_color').value));
	spotLight.position.set(0, 0, 50);
	spotLight.castShadow = false;
	camera.add(spotLight);

	// mouse events
	renderer.domElement.addEventListener( 'mousemove', onMouseMove, false );
	renderer.domElement.addEventListener( 'mousedown', onMouseDown, false );
	renderer.domElement.addEventListener( 'mouseup', onMouseUp, false );

	var mousewheelevt=(isFirefox)? "DOMMouseScroll" : "mousewheel"

	if (mousewheelevt=="DOMMouseScroll") 
		renderer.domElement.addEventListener( mousewheelevt, onMousewheelFirefox, false );
	else if (mousewheelevt=="mousewheel")
		renderer.domElement.addEventListener( mousewheelevt, onMousewheel, false );
	
	
	// keyboard events
	window.addEventListener( 'keydown', onKeyPressed, false );

	// resize event
	window.addEventListener( 'resize', onWindowResize, false );

	createInfoText();

	// init raycaster (to detect click on shell)
	raycaster = new THREE.Raycaster();
	mouse = new THREE.Vector2();

	// div with shell info
	shellDescriptionWindow = document.getElementById('shell_desc');
}

// animation frames
function animateScene(){
	// setup FPS limit
	var fpsLimit = 30;			// <- comment to disable limit fps
	setTimeout( function() {	// <- comment to disable limit fps
		requestAnimationFrame( animateScene );
		renderScene();
	}, 1000 / fpsLimit );		// <- comment to disable limit fps

	date = new Date(); // get current date
	if(date.getTime()-lastMouseMove>DESC_WINDOW_DELAY && !mouseDown && !debugMode) {
		mouse.x = ( mousePosition[0] / renderer.domElement.width ) * 2 - 1;
		mouse.y = - ( mousePosition[1] / renderer.domElement.height ) * 2 + 1;
		raycaster.setFromCamera( mouse, camera );
		var intersects = raycaster.intersectObjects(forams, true);
		if(intersects.length) {
			// if mouse hasn't moved for 0.5 s and it is over shell - show description window
			shellDescriptionWindow.style.display = "inline";
			shellDescriptionWindow.innerHTML = intersects[0].object.userData.p.userData.desc;
			shellDescriptionWindow.style.left = (20+mousePosition[0]).toString()+'px';
			shellDescriptionWindow.style.top = mousePosition[1].toString()+'px';
		}
	} else {
		shellDescriptionWindow.style.display = "none";
		shellDescriptionWindow.innerHTML = "";
	}
}

// render scene
function renderScene(){ 
	renderer.render(scene, camera); 
} 

// Remove 'element' from 'list'. Returns true if element is found and removed.
function remove(list, element) {
	var i = list.length;
	while (i--) {
	   if (list[i] === element) {
			list.splice(i, 1);
		   return true;
	   }
	}
	return false;
}

// build and add forams to scene
function addForams(options, amount, drawPath, transpValue, schemaOn) {
	var foramCounter = 0; 

	for(var i=0; i<amount; i+=1) {
		for(var j=0; j<amount; j+=1) {
			var group = buildForam(options[foramCounter][6],options[foramCounter], transpValue, schemaOn);
			setGroupCoordinates(group, amount, i, j);
			group.userData.posOnScreen = [i, j];
			scene.add(group);
			forams.push(group);
			foramCounter++;
		}
	}
	if(drawPath) addMinPaths();
}

function setGroupCoordinates(group, amount, i, j) {
	var pos = calculatePositionOnScreen(amount, i, j);
	if(!debugMode) {
		group.position.x = pos[0];
		group.position.y = pos[1];
		var scaleRatio = Math.min(pos[2]/(group.userData.bbox.max.x-group.userData.bbox.min.x),
								  pos[3]/(group.userData.bbox.max.y-group.userData.bbox.min.y));
		group.scale.x *= scaleRatio;
		group.scale.y *= scaleRatio;
		group.scale.z *= scaleRatio;
		group.userData.x_maxScale = group.scale.x;
		group.userData.y_maxScale = group.scale.y;
		group.userData.z_maxScale = group.scale.z;
	} else {
		group.scale.x = 1;
		group.scale.y = 1;
		group.scale.z = 1;
		group.position.x = 0;
		group.position.y = 0;
	}
	group.rotation.x = 3.14;
	group.rotation.y = 0;
	group.rotation.z = 0;
	group.userData.color = "white";
}

function calculatePositionOnScreen (amount, i, j) {
	var minLeft = -3.5;
	var minBottom = -3.5;
	var maxRight = 3.5;
	var maxTop = 3.5;
	var padding = 0;
	var centerDiff = [0,0];
	switch(amount) {
		case 1: padding = 1.5; break; // 1x1
		case 2: padding = 0.5; break; // 2x2
		case 3: padding = 0.6; break; // 3x3
		case 4: padding = 0.5; break; // 4x4
		default: padding = 0.1;
	}
	var halfBox = [(maxRight - minLeft)/(2*amount), (maxTop - minBottom)/(2*amount)];
	var maxSize = [2*halfBox[0]-padding, 2*halfBox[1]-padding];
	var first = [minLeft+halfBox[0], minBottom+halfBox[1]];
	return [ first[0]+2*i*halfBox[0]+centerDiff[0], first[1]+2*j*halfBox[1]+centerDiff[1], maxSize[0], maxSize[1]];
}

function clearScene() {
	while(forams.length > 0){
		scene.remove(forams.pop());
	}
	forams = [];
}

function addMinPaths(){
	for(var i = 0; i < forams.length; i++){
		var points = forams[i].userData.foram.apertures.length - 1;
		var path = getMinimizationPath(forams[i].userData.foram, forams[i], points, forams[i].userData.shift);
		path.userData.p = forams[i];
		forams[i].add(path);
	}
}

function removeMinPaths(){
	for(var i = 0; i < forams.length; i++){
		forams[i].remove(forams[i].children[forams[i].children.length-1]);
	}
}

function applySchemaColors(forams){
	for(var i = 0; i < forams.length; i++){
		var selectedIndex = selectedShells.indexOf(forams[i]);
		for(var j = 0; j < forams[i].children.length; j++){
			var child = forams[i].children[j];
			if(child.userData.type == 'chamber') {
				if(selectedIndex < 0) child.material.color.setHex(schemaColors[j % schemaColors.length]);
				else child.material.color.setHex(0x00ff00);
			}
		}
	}
}

function setDefaultColorSchema(forams){
	for(var i = 0; i < forams.length; i++){
		var color = 0xffffff;
		if(selectedShells.indexOf(forams[i]) >= 0) color = 0x00ff00;
		for(var j = 0; j < forams[i].children.length; j++){
			var child = forams[i].children[j];
			if(child.userData.type == 'chamber') child.material.color.setHex(color);
		}
	}
}

