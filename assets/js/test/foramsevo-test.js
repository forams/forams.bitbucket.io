var expect = chai.expect;

var params = [0.4, 1.1, 71.0*Math.PI/180.0, 30.0*Math.PI/180.0, 0.5, 0.5];

var ch1ExpectedCenter = [0.0, 0.0000, 0.0000];
var ch1ExpectedRadius = 0.5000;
var ch1ExpectedAperture = [0.5000, 0.0000, 0.0000];
var ch1ExpectedReferenceVector = [0.2200, 0.0000, 0.0000];
var ch1ExpectedDeviationVector = [0.0716, 0.2080, 0.0000];
var ch1ExpectedGrowthVector = [0.0716, 0.1801, 0.1040];
var ch1ExpectedXAxis = [-0.2044, 0.0813, 0.0000];
var ch1ExpectedZAxis = [0.0384, 0.0966, -0.1939];

var ch2ExpectedCenter = [0.5716, 0.1801, 0.1040];
var ch2ExpectedRadius = 0.5500;
var ch2ExpectedAperture = [0.3942, -0.2680, -0.1610];
var ch2ExpectedReferenceVector = [-0.0776, -0.1965, -0.1180];
var ch2ExpectedDeviationVector = [-0.2420, -0.0011, -0.0007];
var ch2ExpectedGrowthVector = [-0.2130, 0.0494, -0.1038];
var ch2ExpectedXAxis = [-0.0126, 0.2072, 0.1244];
var ch2ExpectedZAxis = [-0.1143, -0.1149, 0.1797];

var ch3ExpectedCenter = [0.1812, -0.2186, -0.2647];
var ch3ExpectedRadius = 0.6050;
var ch3ExpectedAperture = [0.7115, -0.3547, -0.0072];
var ch3ExpectedReferenceVector = [0.2326, -0.0636, 0.1128];
var ch3ExpectedDeviationVector = [0.0928, 0.2114, 0.1325];
var ch3ExpectedGrowthVector = [0.0299, 0.1421, 0.2231];
var ch3ExpectedXAxis = [-0.2095, 0.1498, -0.0674];
var ch3ExpectedZAxis = [0.1615, 0.1680, -0.1287];

function calculateChambers(foram, numberOfChambers) {
	for(var i = 0; i < numberOfChambers; i++) foram.calculateNextChamber();
}

describe("Foraminifera's", function() {
  describe("first chamber", function() {
    it("should be centered at expected position", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 1);
      expect(foram.center[0].toFixed(4)).to.equal(ch1ExpectedCenter[0].toFixed(4));
	  expect(foram.center[1].toFixed(4)).to.equal(ch1ExpectedCenter[1].toFixed(4));
	  expect(foram.center[2].toFixed(4)).to.equal(ch1ExpectedCenter[2].toFixed(4));
    });
	
	it("should have expected radius length", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 1);
      expect(foram.radius.toFixed(4)).to.equal(ch1ExpectedRadius.toFixed(4));
    });
	
	it("should have aperture at expected position", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 1);
      expect(foram.aperture[0].toFixed(4)).to.equal(ch1ExpectedAperture[0].toFixed(4));
	  expect(foram.aperture[1].toFixed(4)).to.equal(ch1ExpectedAperture[1].toFixed(4));
	  expect(foram.aperture[2].toFixed(4)).to.equal(ch1ExpectedAperture[2].toFixed(4));
    }); 
	
	it("reference vector should have expected coordinates", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 2);
      expect(foram.referenceVector[0].toFixed(4)).to.equal(ch1ExpectedReferenceVector[0].toFixed(4));
	  expect(foram.referenceVector[1].toFixed(4)).to.equal(ch1ExpectedReferenceVector[1].toFixed(4));
	  expect(foram.referenceVector[2].toFixed(4)).to.equal(ch1ExpectedReferenceVector[2].toFixed(4));
    });
	
	it("deviation vector should have expected coordinates", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 2);
      expect(foram.deviationVector[0].toFixed(4)).to.equal(ch1ExpectedDeviationVector[0].toFixed(4));
	  expect(foram.deviationVector[1].toFixed(4)).to.equal(ch1ExpectedDeviationVector[1].toFixed(4));
	  expect(foram.deviationVector[2].toFixed(4)).to.equal(ch1ExpectedDeviationVector[2].toFixed(4));
    });
	
	it("growth vector should have expected coordinates", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 2);
      expect(foram.growthVector[0].toFixed(4)).to.equal(ch1ExpectedGrowthVector[0].toFixed(4));
	  expect(foram.growthVector[1].toFixed(4)).to.equal(ch1ExpectedGrowthVector[1].toFixed(4));
	  expect(foram.growthVector[2].toFixed(4)).to.equal(ch1ExpectedGrowthVector[2].toFixed(4));
    });
	
	it("X axis vector should have expected coordinates", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 2);
      expect(foram.axisXVector[0].toFixed(4)).to.equal(ch1ExpectedXAxis[0].toFixed(4));
	  expect(foram.axisXVector[1].toFixed(4)).to.equal(ch1ExpectedXAxis[1].toFixed(4));
	  expect(foram.axisXVector[2].toFixed(4)).to.equal(ch1ExpectedXAxis[2].toFixed(4));
    });
	
	it("Z axis vector should have expected coordinates", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 2);
      expect(foram.axisZVector[0].toFixed(4)).to.equal(ch1ExpectedZAxis[0].toFixed(4));
	  expect(foram.axisZVector[1].toFixed(4)).to.equal(ch1ExpectedZAxis[1].toFixed(4));
	  expect(foram.axisZVector[2].toFixed(4)).to.equal(ch1ExpectedZAxis[2].toFixed(4));
    });
  });
  
  describe("second chamber", function() {
    it("should be centered at expected position", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 2);
      expect(foram.center[0].toFixed(4)).to.equal(ch2ExpectedCenter[0].toFixed(4));
	  expect(foram.center[1].toFixed(4)).to.equal(ch2ExpectedCenter[1].toFixed(4));
	  expect(foram.center[2].toFixed(4)).to.equal(ch2ExpectedCenter[2].toFixed(4));
    });
	
	it("should have expected radius length", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 2);
      expect(foram.radius.toFixed(4)).to.equal(ch2ExpectedRadius.toFixed(4));
    });
	
	it("should have aperture at expected position", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 2);
      expect(foram.aperture[0].toFixed(4)).to.equal(ch2ExpectedAperture[0].toFixed(4));
	  expect(foram.aperture[1].toFixed(4)).to.equal(ch2ExpectedAperture[1].toFixed(4));
	  expect(foram.aperture[2].toFixed(4)).to.equal(ch2ExpectedAperture[2].toFixed(4));
    }); 
	
	it("reference vector should have expected coordinates", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 3);
      expect(foram.referenceVector[0].toFixed(4)).to.equal(ch2ExpectedReferenceVector[0].toFixed(4));
	  expect(foram.referenceVector[1].toFixed(4)).to.equal(ch2ExpectedReferenceVector[1].toFixed(4));
	  expect(foram.referenceVector[2].toFixed(4)).to.equal(ch2ExpectedReferenceVector[2].toFixed(4));
    });
	
	it("deviation vector should have expected coordinates", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 3);
      expect(foram.deviationVector[0].toFixed(4)).to.equal(ch2ExpectedDeviationVector[0].toFixed(4));
	  expect(foram.deviationVector[1].toFixed(4)).to.equal(ch2ExpectedDeviationVector[1].toFixed(4));
	  expect(foram.deviationVector[2].toFixed(4)).to.equal(ch2ExpectedDeviationVector[2].toFixed(4));
    });
	
	it("growth vector should have expected coordinates", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 3);
      expect(foram.growthVector[0].toFixed(4)).to.equal(ch2ExpectedGrowthVector[0].toFixed(4));
	  expect(foram.growthVector[1].toFixed(4)).to.equal(ch2ExpectedGrowthVector[1].toFixed(4));
	  expect(foram.growthVector[2].toFixed(4)).to.equal(ch2ExpectedGrowthVector[2].toFixed(4));
    });
	
	it("X axis vector should have expected coordinates", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 3);
      expect(foram.axisXVector[0].toFixed(4)).to.equal(ch2ExpectedXAxis[0].toFixed(4));
	  expect(foram.axisXVector[1].toFixed(4)).to.equal(ch2ExpectedXAxis[1].toFixed(4));
	  expect(foram.axisXVector[2].toFixed(4)).to.equal(ch2ExpectedXAxis[2].toFixed(4));
    });
	
	it("Z axis vector should have expected coordinates", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 3);
      expect(foram.axisZVector[0].toFixed(4)).to.equal(ch2ExpectedZAxis[0].toFixed(4));
	  expect(foram.axisZVector[1].toFixed(4)).to.equal(ch2ExpectedZAxis[1].toFixed(4));
	  expect(foram.axisZVector[2].toFixed(4)).to.equal(ch2ExpectedZAxis[2].toFixed(4));
    });
  });
  
  describe("third chamber", function() {
    it("should be centered at expected position", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 3);
      expect(foram.center[0].toFixed(4)).to.equal(ch3ExpectedCenter[0].toFixed(4));
	  expect(foram.center[1].toFixed(4)).to.equal(ch3ExpectedCenter[1].toFixed(4));
	  expect(foram.center[2].toFixed(4)).to.equal(ch3ExpectedCenter[2].toFixed(4));
    });
	
	it("should have expected radius length", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 3);
      expect(foram.radius.toFixed(4)).to.equal(ch3ExpectedRadius.toFixed(4));
    });
	
	it("should have aperture at expected position", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 3);
      expect(foram.aperture[0].toFixed(4)).to.equal(ch3ExpectedAperture[0].toFixed(4));
	  expect(foram.aperture[1].toFixed(4)).to.equal(ch3ExpectedAperture[1].toFixed(4));
	  expect(foram.aperture[2].toFixed(4)).to.equal(ch3ExpectedAperture[2].toFixed(4));
    }); 
	
	it("reference vector should have expected coordinates", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 4);
      expect(foram.referenceVector[0].toFixed(4)).to.equal(ch3ExpectedReferenceVector[0].toFixed(4));
	  expect(foram.referenceVector[1].toFixed(4)).to.equal(ch3ExpectedReferenceVector[1].toFixed(4));
	  expect(foram.referenceVector[2].toFixed(4)).to.equal(ch3ExpectedReferenceVector[2].toFixed(4));
    });
	
	it("deviation vector should have expected coordinates", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 4);
      expect(foram.deviationVector[0].toFixed(4)).to.equal(ch3ExpectedDeviationVector[0].toFixed(4));
	  expect(foram.deviationVector[1].toFixed(4)).to.equal(ch3ExpectedDeviationVector[1].toFixed(4));
	  expect(foram.deviationVector[2].toFixed(4)).to.equal(ch3ExpectedDeviationVector[2].toFixed(4));
    });
	
	it("growth vector should have expected coordinates", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 4);
      expect(foram.growthVector[0].toFixed(4)).to.equal(ch3ExpectedGrowthVector[0].toFixed(4));
	  expect(foram.growthVector[1].toFixed(4)).to.equal(ch3ExpectedGrowthVector[1].toFixed(4));
	  expect(foram.growthVector[2].toFixed(4)).to.equal(ch3ExpectedGrowthVector[2].toFixed(4));
    });
	
	it("X axis vector should have expected coordinates", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 4);
      expect(foram.axisXVector[0].toFixed(4)).to.equal(ch3ExpectedXAxis[0].toFixed(4));
	  expect(foram.axisXVector[1].toFixed(4)).to.equal(ch3ExpectedXAxis[1].toFixed(4));
	  expect(foram.axisXVector[2].toFixed(4)).to.equal(ch3ExpectedXAxis[2].toFixed(4));
    });
	
	it("Z axis vector should have expected coordinates", function() {
	  var foram = new Foraminifera(params);
	  calculateChambers(foram, 4);
      expect(foram.axisZVector[0].toFixed(4)).to.equal(ch3ExpectedZAxis[0].toFixed(4));
	  expect(foram.axisZVector[1].toFixed(4)).to.equal(ch3ExpectedZAxis[1].toFixed(4));
	  expect(foram.axisZVector[2].toFixed(4)).to.equal(ch3ExpectedZAxis[2].toFixed(4));
    });
  });
});