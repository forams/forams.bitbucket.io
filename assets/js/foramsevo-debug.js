/****************************/
/******** DEBUG MODE ********/
/****************************/

var firstDebugStep = true;
var debuggedForam = null;
var debuggedGroup = null;
var stepCounter = 0;
var debugChamberCount = 0;
var debugMaxChambers = 20;
var debugForamChambers = [];
var debugMinPath = null;
var debugObjectsRadius;

function enterDebugMode(options, drawPath, transpValue){
	debugConsole.value = '';
	debuggedForam = new Foraminifera(options);
	debuggedGroup = new THREE.Object3D();
	debugForamChambers = [];
	stepCounter = 0;
	debugChamberCount = 0;
	firstDebugStep = true;
	setGroupCoordinates(debuggedGroup, 1, 0, 0);
	if(drawPath) addDebugMinPath();
	forams.push(debuggedGroup);
	scene.add(debuggedGroup);
	nextDebugStep(drawPath, transpValue);
}

function nextDebugStep(drawPath, transpValue){
	if(debugChamberCount < debugMaxChambers){
		if(stepCounter == 0){
			if(firstDebugStep){
				debuggedForam.calculateNextChamber();
				debugObjectsRadius = 0.05 * debuggedForam.radius;
			} else {
				debuggedGroup.remove(debuggedGroup.children[debuggedGroup.children.length-6]);
			}
			debugChamberCount++;
			var chamber = getChamberMesh(debuggedForam, transpValue, 0xFFFFFF);
			chamber.userData.p = debuggedGroup;
			debuggedGroup.add(chamber);
			debugForamChambers.push(chamber);
			var center = getCenterMesh(debuggedForam, debugObjectsRadius);
			center.userData.p = debuggedGroup;
			// center.userData.name = "center"; // DEBUG (to remove)
			debuggedGroup.add(center);
			logToConsole('center');
			logToConsole('radius');
		} else if(stepCounter == 1){
			var length = debuggedGroup.children.length - 2;
			if(length >= 6){
				for( var j = 1; j <= 6; j++){
					var object = debuggedGroup.children[length-j];
					debuggedGroup.remove(object);
				}
			}
			var aperture = getApertureMesh(debuggedForam, debugObjectsRadius);
			aperture.userData.p = debuggedGroup;
			// aperture.userData.name = "aperture (in debug mode)"; // DEBUG (to remove)
			debuggedGroup.add(aperture);
			if(!firstDebugStep){
				var axisYVector = getVectorArrowHelper(debuggedForam, debuggedForam.growthVector, debuggedForam.center, debuggedForam.radius, 0x0000FF);
				var axisXVector = getVectorArrowHelper(debuggedForam, debuggedForam.axisXVector, debuggedForam.center, debuggedForam.radius, 0x000000);
				var axisZVector = getVectorArrowHelper(debuggedForam, debuggedForam.axisZVector, debuggedForam.center, debuggedForam.radius, 0x000000);

				// THREE.ArrowHelper is a group of objects
				// so raycaster returns one of its children
				axisYVector.children[0].userData.p = debuggedGroup;
				axisXVector.children[0].userData.p = debuggedGroup;
				axisZVector.children[0].userData.p = debuggedGroup;	

				axisYVector.children[1].userData.p = debuggedGroup;
				axisXVector.children[1].userData.p = debuggedGroup;
				axisZVector.children[1].userData.p = debuggedGroup;

				debuggedGroup.add(axisYVector);
				debuggedGroup.add(axisXVector);
				debuggedGroup.add(axisZVector);
				// axisYVector.userData.name = "axis Y Vector"; // DEBUG (to remove)
				// axisXVector.userData.name = "axis X Vector"; // DEBUG (to remove)
				// axisZVector.userData.name = "axis Z Vector"; // DEBUG (to remove)
				if(drawPath){
					var pathSegment = getPathSegment(debuggedForam.prevAperture, debuggedForam.aperture);
					pathSegment.userData.p = debuggedGroup;
					debuggedGroup.children[0].add(pathSegment);
				}
			}
			logToConsole('aperture');
		} else if(stepCounter == 2){
			if(!firstDebugStep){
				for(var i = 1; i <= 3; i++){
					debuggedGroup.remove(debuggedGroup.children[debuggedGroup.children.length-1]);
				}
			} 
			var referenceAxis = getReferenceAxisLine(debuggedForam);
			referenceAxis.userData.p = debuggedGroup;
			// referenceAxis.userData.name = "referenceAxis"; // DEBUG (to remove)
			debuggedGroup.add(referenceAxis);
		} else if(stepCounter == 3){
			debuggedForam.calculateNextChamber();
			var helperPlane = getHelperPlane(debuggedForam);
			var referenceVector = getVectorArrowHelper(debuggedForam, debuggedForam.referenceVector, debuggedForam.prevAperture, 0, 0x008000);
			helperPlane.userData.p = debuggedGroup;
			referenceVector.userData.p = debuggedGroup;
			// referenceVector.userData.name = "helper plane"; // DEBUG (to remove)
			debuggedGroup.add(helperPlane);
			debuggedGroup.add(referenceVector);
			logToConsole('reference');
		} else if(stepCounter == 4){
			var deviationVector = getVectorArrowHelper(debuggedForam, debuggedForam.deviationVector, debuggedForam.prevAperture, 0, 0xFF6633);
			deviationVector.userData.p = debuggedGroup;
			// deviationVector.userData.name = "deviation vector"; // DEBUG (to remove)
			debuggedGroup.add(deviationVector);
			logToConsole('deviation');
		} else if(stepCounter == 5){
			var growthVector = getVectorArrowHelper(debuggedForam, debuggedForam.growthVector, debuggedForam.prevAperture, 0, 0x0000FF);
			var axisXVector = getVectorArrowHelper(debuggedForam, debuggedForam.axisXVector, debuggedForam.prevAperture, 0, 0x000000);
			var axisZVector = getVectorArrowHelper(debuggedForam, debuggedForam.axisZVector, debuggedForam.prevAperture, 0, 0x000000);
			growthVector.userData.p = debuggedGroup;
			axisXVector.userData.p = debuggedGroup;
			axisZVector.userData.p = debuggedGroup;
			growthVector.userData.name = "growth vector";
			axisXVector.userData.name = "axis x vector";
			axisZVector.userData.name = "axis z vector";
			debuggedGroup.add(growthVector);
			debuggedGroup.add(axisXVector);
			debuggedGroup.add(axisZVector);
			logToConsole('growth');
			logToConsole('axisX');
			logToConsole('axisZ');
		}
		stepCounter = (stepCounter + 1) % 6;
	}
}

function addDebugMinPath(){
	var points;
	if(stepCounter == 2 || stepCounter == 3) points = debuggedForam.apertures.length - 1;
	else points = debuggedForam.apertures.length - 2;
	debugMinPath = getMinimizationPath(debuggedForam, debuggedGroup, points, null);
	debugMinPath.userData.p = debuggedGroup;
	debuggedGroup.add(debugMinPath);
	var object = debuggedGroup.children.pop();
	debuggedGroup.children.unshift(object);
}

function logToConsole(type){
	var chamberNumber = 'Chamber_' + debugChamberCount;
	if(type == 'center') {
		var center = debuggedForam.center;
		debugConsole.value += chamberNumber + '/Center:\n' + '[' + round(center[0], 4) + ', ' + round(center[1], 4) + ', ' + round(center[2], 4) + ']\n';
	}
	else if(type == 'radius'){
		var radius = debuggedForam.radius;
		debugConsole.value += chamberNumber + '/Radius:\n' + round(radius, 4) + '\n';
	} 
	else if(type == 'aperture'){
		var aperture = debuggedForam.aperture;
		debugConsole.value += chamberNumber + '/Aperture:\n' + '[' + round(aperture[0], 4) + ', ' + round(aperture[1], 4) + ', ' + round(aperture[2], 4) + ']\n';
	}
	else if(type == 'reference'){
		var referenceVector = debuggedForam.referenceVector;
		debugConsole.value += chamberNumber + '/Reference Vector:\n' + '[' + round(referenceVector[0], 4) + ', ' + round(referenceVector[1], 4) + ', ' + round(referenceVector[2], 4) + ']\n';
	}
	else if(type == 'deviation'){
		var deviationVector = debuggedForam.deviationVector;
		debugConsole.value += chamberNumber + '/Deviation Vector:\n' + '[' + round(deviationVector[0], 4) + ', ' + round(deviationVector[1], 4) + ', ' + round(deviationVector[2], 4) + ']\n';
	}
	else if(type == 'growth'){
		var growthVector = debuggedForam.growthVector;
		debugConsole.value += chamberNumber + '/Growth Vector:\n' + '[' + round(growthVector[0], 4) + ', ' + round(growthVector[1], 4) + ', ' + round(growthVector[2], 4) + ']\n';
	}
	else if(type == 'axisX'){
		var axisXVector = debuggedForam.axisXVector;
		debugConsole.value += chamberNumber + '/X Axis Vector:\n' + '[' + round(axisXVector[0], 4) + ', ' + round(axisXVector[1], 4) + ', ' + round(axisXVector[2], 4) + ']\n';
	}
	else if(type == 'axisZ'){
		var axisZVector = debuggedForam.axisZVector;
		debugConsole.value += chamberNumber + '/Z Axis Vector:\n' + '[' + round(axisZVector[0], 4) + ', ' + round(axisZVector[1], 4) + ', ' + round(axisZVector[2], 4) + ']\n';
	}
	debugConsole.scrollTop = debugConsole.scrollHeight
}
