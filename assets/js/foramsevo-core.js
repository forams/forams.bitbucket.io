//	*********** FORAMINIFERA MORPHOLOGY ***********  //

var sumX = 0;
var sumY = 0;
var sumZ = 0;
var content = "";
var radiusScale = 10.0;

// Foraminifera class that allows to calculate
// next chambers' parameters

function Foraminifera (options){
	this.isFirstChamber = true;
	this.isFirstStep = true;
	this.isSecondStep = false;
	this.expansionRatio = options[1];
	this.radius = options[4];
	this.prevRadius = options[4];
	this.translationRatio = options[0];	
	this.fi = options[2];		
	this.beta = options[3];
	this.thickness = options[5];
	this.center = [0.0, 0.0, 0.0];
	this.prevCenter = [0.0, 0.0, 0.0];
	this.aperture = [options[4], 0.0, 0.0];
	this.prevAperture = [0.0, 0.0, 0.0];
	this.prev2Aperture = [0.0, 0.0, 0.0];
	this.prev3Aperture = [0.0, 0.0, 0.0];
	this.apertures = [];
	this.options = options;
	this.centers = [];
	this.radiuses = [];
	this.deviationVector = [0.0, 0.0, 0.0];
	this.growthVector = [0.0, 0.0, 0.0];
	this.referenceVector = [0.0, 0.0, 0.0];
	this.axisXVector = [0.0, 0.0, 0.0];
	this.axisZVector = [0.0, 0.0, 0.0];
	this.size = [[-this.radius,this.radius],[-this.radius,this.radius],[-this.radius,this.radius]];



	this.calculateNextChamber = function(){
		this.setNextRadius();
		this.setNextSphereCenter();
		this.setNextAperture();
		this.updateSize();
	}
	
	// Method that sets length of next chamber's radius

	this.setNextRadius = function() {
		if(this.isFirstChamber){
			return;
		}
		this.prevRadius = this.radius;
		this.radius *= this.expansionRatio;
		this.radiuses.push(this.prevRadius);
	}

	// Method that sets coordinates of next chamber's center
	
	this.setNextSphereCenter = function() {
		if(this.isFirstChamber){
			return;
		}
		this.prevCenter = [this.center[0], this.center[1], this.center[2]];	
		this.prev3Aperture = [this.prev2Aperture[0], this.prev2Aperture[1], this.prev2Aperture[2]];
		this.prev2Aperture = [this.prevAperture[0], this.prevAperture[1], this.prevAperture[2]];
		this.prevAperture = [this.aperture[0], this.aperture[1], this.aperture[2]];	
		var referenceVector = new THREE.Vector3(this.prevAperture[0] - this.prev2Aperture[0], this.prevAperture[1] - this.prev2Aperture[1], this.prevAperture[2] - this.prev2Aperture[2]);
		var expectedGrowthVectorLength = this.radius * this.translationRatio;
		referenceVector.setLength(expectedGrowthVectorLength);
		this.referenceVector = [referenceVector.x, referenceVector.y, referenceVector.z];
		var growthVector = new THREE.Vector3(referenceVector.x, referenceVector.y, referenceVector.z);
		var growthHelperVector = null;
		if(!this.isFirstStep){
			growthHelperVector = new THREE.Vector3(this.prev3Aperture[0] - this.prev2Aperture[0], this.prev3Aperture[1] - this.prev2Aperture[1], this.prev3Aperture[2] - this.prev2Aperture[2]);
		} else {
			growthHelperVector = new THREE.Vector3(0.0, 1.0, 0.0);
			this.isFirstStep = false;
		}
		var normalPlaneVector = new THREE.Vector3(0,0,0);
		normalPlaneVector.crossVectors(growthVector, growthHelperVector);
		normalPlaneVector.normalize();
		growthVector.applyAxisAngle(normalPlaneVector, this.fi);
		growthVector.setLength(expectedGrowthVectorLength);
		this.deviationVector = [growthVector.x, growthVector.y, growthVector.z];
		referenceVector.normalize();
		growthVector.applyAxisAngle(referenceVector, this.beta);
		this.growthVector = [growthVector.x, growthVector.y, growthVector.z];
		this.center = [this.prevAperture[0] + growthVector.x, this.prevAperture[1] + growthVector.y, this.prevAperture[2] + growthVector.z];
		this.centers.push([this.prevCenter[0], this.prevCenter[1], this.prevCenter[2]]);
		
		var axisXVector = new THREE.Vector3(0,0,0);
		axisXVector.crossVectors(normalPlaneVector, growthVector);
		axisXVector.setLength(expectedGrowthVectorLength);
		this.axisXVector = [axisXVector.x, axisXVector.y, axisXVector.z];
		
		var axisZVector = new THREE.Vector3(0,0,0);
		axisZVector.crossVectors(axisXVector, growthVector);
		axisZVector.setLength(expectedGrowthVectorLength);
		this.axisZVector = [axisZVector.x, axisZVector.y, axisZVector.z];
	}
	
	// Method that sets coordinates of next chamber's aperture
	
	this.setNextAperture = function() {
		if(this.isFirstChamber){
			this.apertures.push([this.aperture[0], this.aperture[1], this.aperture[2]]);
			this.isFirstChamber = false;
			return;
		}
		var sphereLines = 100;
		var candidate = [0.0, 0.0, 0.0];
		var shortestDistance = 10000000;
		for (var i = 0; i <= sphereLines; i++) {
			for (var j = 0; j <= sphereLines; j++){
				var X = this.center[0] + (this.radius * Math.sin(Math.PI * j / sphereLines) * Math.cos(2 * Math.PI * i / sphereLines));
				var Y = this.center[1] + (this.radius * Math.sin(Math.PI * j / sphereLines) * Math.sin(2 * Math.PI * i / sphereLines));
				var Z = this.center[2] + (this.radius * Math.cos(Math.PI * j / sphereLines));
				
				var distanceToAperture = Math.sqrt(Math.pow(X - this.prevAperture[0], 2) + Math.pow(Y - this.prevAperture[1], 2) + Math.pow(Z - this.prevAperture[2], 2));
				var insideSphere = false;
				if(distanceToAperture < shortestDistance){
					for(var k = 0; k < this.centers.length; k++){
						var distanceToCentre = Math.sqrt(Math.pow(X - this.centers[k][0], 2) + Math.pow(Y - this.centers[k][1], 2) + Math.pow(Z - this.centers[k][2], 2));
						if(distanceToCentre <= this.radiuses[k]){
							insideSphere = true;
							break;
						}
					}
					if(!insideSphere){
						shortestDistance = distanceToAperture;
						candidate = [X, Y, Z];
					}						
				}
			}	
		}
		this.aperture = [candidate[0], candidate[1], candidate[2]];
		this.apertures.push([this.aperture[0], this.aperture[1], this.aperture[2]]);
	}
	
	this.calculatePathProportion = function(){
		if(this.centers.length != 0){
			var euclideanDistance = Math.sqrt(Math.pow(this.aperture[0] - this.apertures[0][0],2) + Math.pow(this.aperture[1] - this.apertures[0][1],2) + Math.pow(this.aperture[2] - this.apertures[0][2],2));
			var segmentSumDistance = 0.0;
			for(var i = 0; i < this.apertures.length - 1; i++){
				segmentSumDistance += Math.sqrt(Math.pow(this.apertures[i][0] - this.apertures[i+1][0],2) + Math.pow(this.apertures[i][1] - this.apertures[i+1][1],2) + Math.pow(this.apertures[i][2] - this.apertures[i+1][2],2));
			}
			return euclideanDistance/segmentSumDistance;
		}
		return 0.0;
	}

	this.updateSize = function() {
		var tmp;
		// update shell size each time new chamber is added
		// left X
		tmp = this.center[0]-this.radius;
		if(this.size[0][0] > tmp) this.size[0][0]=tmp;
		// right X
		tmp = this.center[0]+this.radius;
		if(this.size[0][1] < tmp) this.size[0][1]=tmp;
		// bottom Y
		tmp = this.center[1]-this.radius;
		if(this.size[1][0] > tmp) this.size[1][0]=tmp; 
		// top Y
		tmp = this.center[1]+this.radius;
		if(this.size[1][1] < tmp) this.size[1][1]=tmp; 
		// back Z
		tmp = this.center[2]-this.radius;
		if(this.size[2][0] > tmp) this.size[2][0]=tmp;
		// front Z
		tmp = this.center[2]+this.radius;
		if(this.size[2][1] < tmp) this.size[2][1]=tmp; 
	}

	this.calculateTotalVolume = function() {
		var pointsPerDim = 100;
		var pointsInsideCuboid = 0;
		var pointsInsideShell = 0;

		// itarate over points inside cuboid surrounding shell (pointsPerDim X pointsPerDim X pointsPerDim)
		var stepX = 1.0*(this.size[0][1]-this.size[0][0])/pointsPerDim;
		var stepY = 1.0*(this.size[1][1]-this.size[1][0])/pointsPerDim;
		var stepZ = 1.0*(this.size[2][1]-this.size[2][0])/pointsPerDim;
		for(var x=this.size[0][0]; x<this.size[0][1]; x+=stepX) {
			for(var y=this.size[1][0]; y<this.size[1][1]; y+=stepY) {
				for(var z=this.size[2][0]; z<this.size[2][1]; z+=stepZ) {
					pointsInsideCuboid++;
					var isInside = false;
					// is point inside chambers (except last one)?
					for(var ch = 0; ch<this.centers.length; ch++) {
						var distance = Math.sqrt(Math.pow(this.centers[ch][0]-x, 2) + Math.pow(this.centers[ch][1]-y, 2) + Math.pow(this.centers[ch][2]-z, 2));
						if(distance<this.radiuses[ch]) {
							// point is in one of the chambers...
							pointsInsideShell++;
							isInside = true;
							break; // ...so no need to iterate over rest of them
						}
					}
					// is point inside last chamber?
					if(!isInside) {
						var distance = Math.sqrt(Math.pow(this.center[0]-x, 2) + Math.pow(this.center[1]-y, 2) + Math.pow(this.center[1]-z, 2));
						if(distance<this.radius)
							pointsInsideShell++;
					}
				}
			}
		}
		var cuboidVolume = (this.size[0][1]-this.size[0][0])*(this.size[1][1]-this.size[1][0])*(this.size[2][1]-this.size[2][0]); // x*y*z
		var volume = (1.0*pointsInsideShell/pointsInsideCuboid)*cuboidVolume*Math.pow(radiusScale, 3);
		// console.log("LOG: total points: "+pointsInsideCuboid);
		// console.log("LOG: points inside shell: "+pointsInsideShell);
		return volume;
	}

	this.calculateMaterialVolumeAndArea = function() {
		var chambersToTestCenter = [];
		var chambersToTestRadius = [];
		var currentChamberIndex = -1; // start with 1st chamber
		var areaForVolume = 0.0;
		var area = 0.0;
		var sphereLines = 180;

		// those lists contain all chambers including the last one
		var completeListOfCenters = [];
		var completeListOfRadiuses = [];
		// copy list
		for(var i=0; i<this.centers.length; i++) {
			completeListOfCenters.push(this.centers[i]);
			completeListOfRadiuses.push(this.radiuses[i])
		}
		// add last chamber
		completeListOfCenters.push(this.center);
		completeListOfRadiuses.push(this.radius)
		
		// for each chamber starting with 1st
		for(var ch=0; ch<completeListOfCenters.length; ch++) {
			var totalPoints = 0;
			var includedPointsVolume = 0;
			var includedPointsArea = 0;

			// for each point on chamber
			for (var i = 0; i <= sphereLines; i++) {
				for (var j = 0; j <= sphereLines; j++){
					var X = completeListOfCenters[ch][0] + (completeListOfRadiuses[ch] * Math.sin(Math.PI * j / sphereLines) * Math.cos(2 * Math.PI * i / sphereLines));
					var Y = completeListOfCenters[ch][1] + (completeListOfRadiuses[ch] * Math.sin(Math.PI * j / sphereLines) * Math.sin(2 * Math.PI * i / sphereLines));
					var Z = completeListOfCenters[ch][2] + (completeListOfRadiuses[ch] * Math.cos(Math.PI * j / sphereLines));
					
					//
					//		CALCULATING MATERIAL VOLUME
					//
					var isInside = false;
					// check if point is inside any prevoius chambers
					// for 1st chamber there aren't any previous chambers, so skip loop
					// for 2nd chamber we only need to check points that lays inside 1st chamber
					// for 3rd chamber we need to check both 1st and 2nd chamber
					// ...
					for(var otherChamber=0; otherChamber<chambersToTestCenter.length; otherChamber++) {
						var distance = Math.sqrt(Math.pow(X - chambersToTestCenter[otherChamber][0], 2) +
												 Math.pow(Y - chambersToTestCenter[otherChamber][1], 2) +
												 Math.pow(Z - chambersToTestCenter[otherChamber][2], 2));
						if(distance<chambersToTestRadius[otherChamber]) {
							isInside = true;
							break; // if it's inside in any chamber, we dont need to check anymore
						}
					}
					if(!isInside)
						includedPointsVolume++;

					//
					//		CALCULATING AREA
					//
					var isInside2 = false;
					for(var otherChamber=0; otherChamber<completeListOfCenters.length; otherChamber++) {
						var distance = Math.sqrt(Math.pow(X - completeListOfCenters[otherChamber][0], 2) +
												 Math.pow(Y - completeListOfCenters[otherChamber][1], 2) +
												 Math.pow(Z - completeListOfCenters[otherChamber][2], 2));
						if(distance<completeListOfRadiuses[otherChamber]) {
							isInside2 = true;
							break; // if it's inside in any chamber, we dont need to check anymore
						}
					}
					if(!isInside2)
						includedPointsArea++;

					totalPoints++;
				}	
			}
			var sphereArea = 4*Math.PI*completeListOfRadiuses[ch]*completeListOfRadiuses[ch]; // total area of current chamber
			areaForVolume += (1.0*includedPointsVolume/totalPoints)*sphereArea; // points ratio * area
			area += (1.0*includedPointsArea/totalPoints)*sphereArea;
			chambersToTestCenter.push(completeListOfCenters[currentChamberIndex+1]);	
			chambersToTestRadius.push(completeListOfRadiuses[currentChamberIndex++]); // for next chamber we need to include current one

		}
		area *= Math.pow(radiusScale, 2);
		volume = areaForVolume * this.thickness * Math.pow(radiusScale, 2); // TODO change 1.0 to thickness once it's implemented
		return [volume, area];
	}
}

// Function that returns a 3D object representing Foraminifera with given parameters
function buildForam(numOfChambers, options, transpValue, schemaOn) {
	var foram = new Foraminifera(options);
	var group = new THREE.Object3D(); 
	group.userData.array = [];
	var color = 0xFFFFFF;
	for(i = 0; i<numOfChambers; i++) {
		foram.calculateNextChamber();
		if(schemaOn) color = schemaColors[i % schemaColors.length];
		var chamber = getChamberMesh(foram, transpValue, color);
		chamber.userData.type = 'chamber';
		chamber.userData.p = group;
		group.add(chamber);
		group.userData.array.push(chamber);
	}
	var apertureRadius = 0.05 * foram.radius;
	var aperture = getApertureMesh(foram, apertureRadius);
	aperture.userData.p = group;
	// aperture.userData.name = "aperture"; // DEBUG (to remove)
	group.add(aperture);
	group.userData.foram = foram;
	group.userData.desc =   "PARAMETERS:<br>"+
							"TF: "+round(group.userData.foram.options[0], 4)+"<br/>"+
							"GF: "+round(group.userData.foram.options[1], 4)+"<br/>"+
							"&Delta;&Phi;&deg;: "+round(group.userData.foram.options[2]*180.0/Math.PI, 4)+"<br/>"+
							"&beta;&deg;: "+round(group.userData.foram.options[3]*180.0/Math.PI, 4)+"<br/>"+
							"R[&#181;m]: "+round(group.userData.foram.options[4]*radiusScale, 4)+"<br/>"+
							"Thick.[&#181;m]: "+round(group.userData.foram.options[5], 4)+"<br/>";

	
	group.userData.bbox = new THREE.Box3().setFromObject(group);

	sumX = group.userData.bbox.min.x + (group.userData.bbox.max.x - group.userData.bbox.min.x)/2;
    sumY = group.userData.bbox.min.y + (group.userData.bbox.max.y - group.userData.bbox.min.y)/2;
    sumZ = group.userData.bbox.min.z + (group.userData.bbox.max.z - group.userData.bbox.min.z)/2;

	group.userData.shift = [sumX, sumY, sumZ];

	for(var c=0; c<group.children.length; c++) {
		group.children[c].position.x += -sumX;
		group.children[c].position.y += -sumY;
		group.children[c].position.z += -sumZ;
	}
	
	sumX = 0;
	sumY = 0;
	sumZ = 0;

	return group;	
}

// Function that crossbreeds selected Foraminiferas and returns desired number of children
// parents[0] - first parent, parents[1] - second parent

function crossbreedForam(parents, childrenNumber){
	var children = [];
	for(var i = 0; i < childrenNumber; i++){
		var child = [];
		for(var j = 0; j < paramNumber; j++){
			child.push(parents[Math.round(Math.random())][j]);
		}
		children.push(child);
	}
	generation += 1;
	content = "Generation: " + generation;
	document.getElementById("generation").innerHTML=content; 
	return children;
}

// Function that mutates selected Foraminifera with given mutation parameters
// and returns desired number of children
// parameters order - TF, GF, fi, beta

function mutateForam(parent, childrenNumber){
	var mutationRates = getMutationRates();
	var minValues = [0.0, 1.0, 0.0, 0.0, 0.0, 0.0];
	var maxValues = [1.0, 2.0, 2 * Math.PI, 2 * Math.PI, 2.0];
	var children = [];
	var diff = 0.0;
	var sign = 0;
	var reference;
	for(var i = 0; i < childrenNumber; i++){
		var child = [];
		for(var j = 0; j < paramNumber - 1; j++){
			if(j != 4 && j != 5) reference = maxValues[j] - minValues[j];
			else reference = parent[j];
			diff = Math.random() * mutationRates[j] / 100.0 * reference;
			sign = Math.round(Math.random());
			if((sign == 0 || parent[j] - diff < minValues[j]) && parent[j] + diff < maxValues[j]){
				child.push(parent[j] + diff);
				if(j == 4) maxValues.push(parent[j] + diff);
			}else{
				child.push(parent[j] - diff);
				if(j == 4) maxValues.push(parent[j] - diff);
			}
		}
		child.push(parent[paramNumber-1]);
		children.push(child);
		maxValues.pop();
	}
	generation += 1;
	content = "Generation: " + generation;
	document.getElementById("generation").innerHTML=content; 
	updateSelectButton();
	return children;
}

//////////////////////// FORAMINIFERA 3D PARTS ////////////////////////////

// Functions that return 3D objects representing parts of Foraminifera
// or helper debug objects

function drawLine(vector1, vector2) {
	var geometry = new THREE.Geometry();
	var material = new THREE.LineBasicMaterial({ color: 0x880000});
	geometry.vertices.push(
		vector1,
		vector2);
	var line = new THREE.Line( geometry, material );
	return line;
}

function getChamberMesh(foram, transpValue, chamberColor){
	var chamberRadius = foram.radius; 
	var chamberCenter = foram.center;
	var chamberGeometry = new THREE.SphereGeometry(chamberRadius, 32, 32 );
	var chamberMaterial = new THREE.MeshLambertMaterial( {color: chamberColor, transparent: true, opacity: transpValue} );
	var chamber = new THREE.Mesh( chamberGeometry, chamberMaterial );
	chamber.position.x = chamberCenter[0];
	chamber.position.y = chamberCenter[1];
	chamber.position.z = chamberCenter[2];
	sumX += chamberCenter[0];
	sumY += chamberCenter[1];
	sumZ += chamberCenter[2];
	return chamber;
}

function getApertureMesh(foram, radius){
	var apertureGeometry = new THREE.SphereGeometry(radius, 32, 32 );
	var apertureMaterial = new THREE.MeshBasicMaterial({ color: 0x000000 });
	var aperture = new THREE.Mesh( apertureGeometry, apertureMaterial );
	var aperturePosition = foram.aperture;
	aperture.position.set( aperturePosition[0], aperturePosition[1], aperturePosition[2] );
	return aperture;
}

function getCenterMesh(foram, radius){
	var centerGeometry = new THREE.SphereGeometry(radius, 32, 32 );
	var centerMaterial = new THREE.MeshBasicMaterial({ color: 0xFF0000 });
	var center = new THREE.Mesh( centerGeometry, centerMaterial );
	var centerPosition = foram.center;
	center.position.set( centerPosition[0], centerPosition[1], centerPosition[2] );
	return center;
}

function getReferenceAxisLine(foram){
	var geometry = new THREE.Geometry();
	var material = new THREE.LineBasicMaterial({ color: 0x008000, linewidth: 3 });
	var prevAperturePosition = foram.prevAperture;
	var aperturePosition = foram.aperture;
	var vector = [2.5 * (aperturePosition[0] - prevAperturePosition[0]), 2.5 * (aperturePosition[1] - prevAperturePosition[1]), 2.5 * (aperturePosition[2] - prevAperturePosition[2])];
	var reversedVector = [-1 * vector[0], -1 * vector[1], -1 * vector[2]];
	geometry.vertices.push(
		new THREE.Vector3(prevAperturePosition[0] + reversedVector[0], prevAperturePosition[1] + reversedVector[1], prevAperturePosition[2] + reversedVector[2]),
		new THREE.Vector3(aperturePosition[0] + vector[0], aperturePosition[1] + vector[1], aperturePosition[2] + vector[2]));
	var line = new THREE.Line(geometry, material);
	return line;
}

function getMinimizationPath(foram, group, points, shift){
	var path = new THREE.Object3D();
	for(var i = 0; i < points; i++){
		var aperture1 = [foram.apertures[i][0], foram.apertures[i][1], foram.apertures[i][2]];
		var aperture2 = [foram.apertures[i+1][0], foram.apertures[i+1][1], foram.apertures[i+1][2]];
		if(shift){
			for(var j = 0; j < 3; j++){
				aperture1[j] += - shift[j];
				aperture2[j] += - shift[j];
			}
		}
		var geometry = new THREE.Geometry();
		var material = new THREE.LineBasicMaterial({ color: 0xFF0000, linewidth: 3 });
		geometry.vertices.push(
		new THREE.Vector3(aperture1[0], aperture1[1], aperture1[2]),
		new THREE.Vector3(aperture2[0], aperture2[1], aperture2[2]));
		var line = new THREE.Line(geometry, material);
		line.userData.p = group;
		path.add(line);
	}
	return path;
}

function getPathSegment(start, end){
	var geometry = new THREE.Geometry();
	var material = new THREE.LineBasicMaterial({ color: 0xFF0000, linewidth: 3 });
	geometry.vertices.push(
	new THREE.Vector3(start[0], start[1], start[2]),
	new THREE.Vector3(end[0], end[1], end[2]));
	var line = new THREE.Line(geometry, material);
	return line;
}

function getVectorArrowHelper(foram, vector, location, length, color){
	var vectorBeg = new THREE.Vector3( location[0], location[1], location[2] );
	var vectorEnd = new THREE.Vector3( location[0] + vector[0], location[1] + vector[1], location[2] + vector[2] );
	var direction = vectorEnd.clone().sub(vectorBeg);
	var vectorLength = length;
	if(vectorLength == 0) vectorLength = direction.length()
	var arrowHelper = new THREE.ArrowHelper(direction.normalize(), vectorBeg, vectorLength, color );
	arrowHelper.line.material.linewidth = 3;
	return arrowHelper;
}

function getHelperPlane(foram){
	var geometry = new THREE.Geometry();
	var material = new THREE.MeshBasicMaterial({ side:THREE.DoubleSide, color: 0xFFFFFF });
	if(!firstDebugStep){
		var radius = foram.radius;
		var prevAperture = foram.prevAperture;
		var prev2Aperture = foram.prev2Aperture;
		var prev3Aperture = foram.prev3Aperture;
		var vector1 = new THREE.Vector3(prev2Aperture[0] - prevAperture[0],prev2Aperture[1] - prevAperture[1],prev2Aperture[2] - prevAperture[2]);
		var vector2 = new THREE.Vector3(prev3Aperture[0] - prevAperture[0],prev3Aperture[1] - prevAperture[1],prev3Aperture[2] - prevAperture[2]);
		vector1.setLength(radius);
		vector2.setLength(radius);
		var point1 = new THREE.Vector3(vector1.x + prevAperture[0],vector1.y + prevAperture[1],vector1.z + prevAperture[2]);
		var point2 = new THREE.Vector3(vector2.x + prevAperture[0],vector2.y + prevAperture[1],vector2.z + prevAperture[2]);
		var point3 = new THREE.Vector3(prevAperture[0] - vector1.x,prevAperture[1] - vector1.y,prevAperture[2] - vector1.z);
		var point4 = new THREE.Vector3(prevAperture[0] - vector2.x,prevAperture[1] - vector2.y,prevAperture[2] - vector2.z);
		geometry.vertices.push(new THREE.Vector3(prevAperture[0],prevAperture[1],prevAperture[2]));
		geometry.vertices.push(point1);
		geometry.vertices.push(point2);
		geometry.vertices.push(point3);
		geometry.vertices.push(point4);
	} else {
		var prevRadius = foram.prevRadius;
		var radius = foram.radius;
		geometry.vertices.push(new THREE.Vector3(prevRadius,0,0));
		geometry.vertices.push(new THREE.Vector3(prevRadius + radius, radius,0));
		geometry.vertices.push(new THREE.Vector3(prevRadius + radius, -1 * radius,0));
		geometry.vertices.push(new THREE.Vector3(prevRadius - radius, -1 * radius,0));
		geometry.vertices.push(new THREE.Vector3(prevRadius - radius, radius,0));
		firstDebugStep = false;
	}
	geometry.faces.push( new THREE.Face3( 0, 1, 2 ) );
	geometry.faces.push( new THREE.Face3( 0, 3, 4 ) );
	geometry.faces.push( new THREE.Face3( 0, 1, 4 ) );
	geometry.faces.push( new THREE.Face3( 0, 2, 3 ) );
	geometry.dynamic = true;
	var planeMesh = new THREE.Mesh(geometry, material);
	return planeMesh;
}

////////////////////////////////////////////////////////////////////////////////////